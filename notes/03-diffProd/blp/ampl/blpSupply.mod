# AMPL model file for BLP demand estimation with supply side
# Paul Schrimpf
# 5 November, 2008

# We assume Bertrand competition with each product produced 
# by a different firm.  

# data 
param Kp; # product characteristics
param Ki; # individual characterisitcs
param J;  # number of products
param N;  # number of individuals (only observe their characteristics)
param S;  # number of draws of random coefs (and from observed char dist)
param M;  # number of markets 
param Kz; # number of demand instruments
param Kc; # number of cost shifters
param Kzc; # number of cost instruments
param c{1..J,1..M,1..Kc}; # cost shitters (notation slightly diff than in notes)
param x{1..J,1..M,1..Kp}; 
param w{1..N,1..M,1..Ki}; # individual characteristics (notation slightly diff than in notes)
param yo{1..N,1..M}; # observed income distribution
param z{1..J,1..M,1..Kz}; # demand instruments 
param zc{1..J,1..M,1..Kzc}; # supply instruments
param s{1..J,1..M}; # product shares
param W{1..Kz,1..Kz,1..M,1..M}; # demand weighting matrix
param Wc{1..Kzc,1..Kzc,1..M,1..M}; # supply weighting matrix
 # Note: assuming block diagonal weighting between demand and supply
param nu{1..S,1..M,1..Kp}; # draws for random coefs
param d{1..S,1..M,1..Ki};  # draws for individual characteristics
param y{1..S,1..M}; # simulated income distribution

var p{1..J, 1..M} >= 0; # prices
 # if we had real data, prices would be a parameter, but we're simulating
 # so we need to solve for prices, so they're a variable
param etaSim{1..J,1..M};

# variables (things that will be solved for)
var delta{1..J,1..M}; 
var alpha; # price coef 
var beta{1..Kp}; # mean coefs on x
var pi{1..Kp,1..Ki}; # x*w coefs
var sig{1..Kp}>=0.1; # std dev of coefs on x0
var alphac{1..Kc}; # coefs on c

################################################################
# parts of computation
var omega{j in 1..J,m in 1..M} # aka xi in notes
  = (delta[j,m] - sum{k in 1..Kp} x[j,m,k]*beta[k]);
var mu{j in 1..J,m in 1..M,i in 1..S} = 
   delta[j,m] + alpha*log(max(1e-307,y[i,m] - p[j,m])) + sum{k in 1..Kp} x[j,m,k]*
   (sig[k]*nu[i,m,k] + sum{ki in 1..Ki} d[i,m,ki]*pi[k,ki]);
var pbuy{i in 1..S,j in 1..J,m in 1..M} = # P(sim i buys product j)
     exp(mu[j,m,i]) 
   / (exp(alpha)*y[i,m] + sum{j2 in 1..J} exp(mu[j2,m,i]));
var shat{j in 1..J, m in 1..M} = # share implied by model
       1/S*sum{i in 1..S} pbuy[i,j,m];
var dsdp{j in 1..J,m in 1..M} = # derivative of share wrt price
  1/S * sum{i in 1..S} -alpha/max(1e-307,y[i,m]-p[j,m])*pbuy[i,j,m]*(1-pbuy[i,j,m]);
var eta{j in 1..J,m in 1..M} = # shocks to marginal cost
  log(max(1e-307,p[j,m] + shat[j,m]/dsdp[j,m]))  # avoid log(0)
  - sum{k in 1..Kc} alphac[k]*c[j,m,k];

################################################################################
# objective function and constraints
minimize moments : 
  sum{m1 in 1..M,kz1 in 1..Kz,m2 in 1..M,kz2 in 1..Kz} 
     (sum{j in 1..J} omega[j,m1]*z[j,m1,kz1])
  *W[kz1,kz2,m1,m2]*
  (sum{j in 1..J} omega[j,m2]*z[j,m2,kz2]) 
  + 
  sum{m1 in 1..M,kz1 in 1..Kzc,m2 in 1..M,kz2 in 1..Kzc} 
  (sum{j in 1..J} eta[j,m1]*zc[j,m1,kz1])
  *Wc[kz1,kz2,m1,m2]*
  (sum{j in 1..J} eta[j,m2]*zc[j,m2,kz2]) 
  ;

subject to shares{j in 1..J,m in 1..M}:
  s[j,m] = shat[j,m];

################################################################################
# The next two constraints are used to solve for prices, but not in estimation
subject to niceP{j in 1..J, m in 1..M}:
  p[j,m] + shat[j,m]/dsdp[j,m] >= 0; # if not, it's impossible to satisfy firm FOC, and eta will be NaN'
  
subject to priceCon{j in 1..J,m in 1..M}:
  p[j,m] = exp((sum{k in 1..Kc} alphac[k]*c[j,m,k])+etaSim[j,m]) 
    - shat[j,m]/dsdp[j,m]; 
