% m-file for making tables and graphs to summarize results of BLP
% estimation
clear;

%% parameters
filename = 'blp.sigBdd.csv'; %'blpSS2.csv';
lalpha = 3; %3; 
nbeta = 1;
nsig = 1;
npi = 1;
nalphac = 1; %2;
good = [1 2];
% all true values are 1 except possibly alpha,
trueAlpha = 1; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data = loadData(filename,lalpha,nbeta,nsig,npi,nalphac,good);
makeTable([filename '.table.tex'],data,trueAlpha);

figure;
hist(data.alpha,20);
print([filename '.alpha.eps'],'-depsc2');
title("\alpha")

figure;
hist(data.beta(:,nbeta),20);
print([filename '.beta.eps'],'-depsc2');
title("\beta")

figure;
hist(data.sig(:,nsig),20);
print([filename '.sig.eps'],'-depsc2');
title("\Sigma")

figure;
hist(data.Pi(:,npi),20);
title("\pi");
print([filename '.sig.eps'],'-depsc2');

if (nalphac>0)
  figure;
  hist(data.alphac(:,nalphac),20);
  print([filename '.alphac.eps'],'-depsc2');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


