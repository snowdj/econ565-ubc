%% create table of results
function makeTable(filename,data,trueAlpha);
  of = fopen(filename,'w');
  fprintf(of,'\\begin{tabular}{c|ccccc}\n');
  fprintf(of, ['Parameter & True Value & Mean & Median & Variance & MSE ' ...
               '\\\\ \\hline \n']); 
  if (~isempty(data.alpha))
    printVar(of,'alpha',data.alpha,trueAlpha);
  end
  names = fieldnames(data);
  for(j = 1:numel(names));
    name = names{j};
    if (strcmp(name,'alpha') || strcmp(name,'raw'))
      continue;
    end
    for(k=1:size(data.(name),2))
      printVar(of,[name num2str(k)],data.(name)(:,k),1);
    end
  end
  fprintf(of,['\\hline \\multicolumn{6}{c}{\\footnotesize{Based on %d ' ...
              'Repititions}} \\\\'],size(data.beta,1));
  fprintf(of,'\\hline \n \\end{tabular}');
  
end

function printVar(of,name,x,true)
  fprintf(of,'%s & %4g & %4g & %4g & %4g & %4g \\\\ \n', ...
          name,true,mean(x),median(x),var(x),mean((x-true).^2));
end

