%% put data into nice struc
function data=loadData(filename,lalpha,nbeta,nsig,npi,nalphac,good)
  data.raw = csvread(filename,1,0);
  success = all(data.raw(:,good)<=1,2);
  if (lalpha) 
    data.alpha = data.raw(success,lalpha);
    l=lalpha+1;
  else 
    data.alpha=[];
    l = good(end)+1;
  end
  data.beta = data.raw(success,l:l+nbeta-1);
  l = l + nbeta;
  data.sig = data.raw(success,l:l+nsig-1);
  l = l+nsig;
  data.Pi = data.raw(success,l:l+npi-1);
  l = l+npi;
  if (nalphac>0) 
    data.alphac = data.raw(success,l:l+nalphac-1);
  else
    data.alphac = [];
  end
end

