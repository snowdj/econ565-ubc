\input{../slideHeader}

\usepackage{pst-tree,array}    
\def\PSB#1{\pspicture(3,1.5)\psTextFrame[shadow,fillcolor=blue!30,
  fillstyle=solid,linecolor=blue,framearc=0.3](0,0)(3,1.5){%
    \shortstack{#1}}\endpspicture}

\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Matching}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Brief review: \citet{fox2009}
  \item Longer review: \citet{graham2011}
  \item Extensive notes: \citet{galichon2011}
  \item Identification: \citet{fox2010id}, \citet{galichon2010}, and
    many of the papers below
  \item Applications
    \begin{itemize}
    \item Marriage: \citet{choo2006} , \citet{galichon2010}
    \item Mergers: \citet{uetake2012}, \citet{park2012}, \citet{akkus2012}
    \item Venture capital: \citet{sorenson2007}
    \item Downstream - upstream firms: \citet{fox2010}
    \item Medical residents: \citet{agarwal2012}
    \end{itemize}
  \end{itemize} 
\end{frame}

\begin{frame}
  \tableofcontents  
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item Matching: payoffs depend on who matches with whom
  \item Examples:
    \begin{itemize}
    \item Firm mergers
    \item Firm upstream/downstream relationships
    \item Workers and firms
    \item Houses for consumers
    \item Marriage
    \end{itemize}
  \item Model primitive: payoffs of all potential matches
  \item Equilibrium: pairwise stability - no couple would prefer to
    deviate 
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item Structural empirical matching models: 
    \begin{itemize}
    \item Data on observed matches and their characteristics
    \item Goal: estimate payoff function
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Types of matching}
  \begin{itemize}
  \item Transferable vs non transferable utility
  \end{itemize}  
  \pstree[treemode=R,levelsep=3.5cm]{\Tr[ref=cc]{\PSB{Matching}}}{
    \pstree[treemode=R,levelsep=3.5cm]{\Tr[ref=cc]{\PSB{One-to-one}}}{
      \Tr[ref=cc]{\PSB{Unilateral\\Preferences\\(housing)}}
      \Tr[ref=cc]{\PSB{Bilateral\\Preferences\\(marriage)}}
    }
    \Tr[ref=cc]{\PSB{Many-to-one \\(workers to firms)}} 
    \Tr[ref=cc]{\PSB{Many-to-many\\(players into teams)}} 
  }
\end{frame}

\begin{frame}\frametitle{Matching -- theory}
  \begin{itemize}
  \item Much more developed than empirical work
  \item Optimal transportation theory -- results imply existence and
    (in some cases) uniqueness of optimal matching; and existence,
    uniqueness, and efficiency of  equilibrium 
  \item See \citet{galichon2011} and references therein
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\citet{sorenson2007}}

\begin{frame}\frametitle{\citet{sorenson2007} ``How  Smart  Is  Smart 
    Money?  A Two-Sided Matching  Model  of Venture  Capital''}
  \begin{itemize}
  \item Fact: companies invested in by more experienced venture
    capitalists are more likely to go public
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[height=0.75\textheight,keepaspectratio=true]{figs/sorenson-fig3}
\end{frame}

\begin{frame}
\frametitle{\citet{sorenson2007} ``How  Smart  Is  Smart 
    Money?  A Two-Sided Matching  Model  of Venture  Capital''}
  \begin{itemize}
  \item Question: is this because experiences VCs invest in better
    companies or because experienced VCs' influence adds value to
    companies? 
  \item Matching model used to distinguish these explanations
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Why matching?}
  \begin{itemize}
  \item VCs affect company value by:
    \begin{itemize}
    \item Monitoring, management
    \item Providing contacts
    \item Signaling value to other investors
    \end{itemize}
  \item Prior evidence that companies care about identity of
    investors; do not simply take best financial offer
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model}
  \begin{itemize}
  \item Set of investors $I$, set of companies $J$
  \item One-to-many: company has one investor; investor many companies
  \item Valuation of match $V_{ij}$
  \item Match correspondence $\mu$
  \item Payoffs: non transferable
    \begin{itemize}
    \item Investor: $\pi_i(\mu(i)) = \lambda \sum_{j \in \mu(i)} V_{ij}$
    \item Company: $\pi_j = (1-\lambda) V_{\mu(j)j}$
    \end{itemize}
  \item Equilibrium: pairwise-stability
    \begin{itemize}
    \item Opportunity cost of deviating for a pair that is not matched
      in $\mu$
      \[ \overline{V}_{ij} \equiv V_{\mu(j)j} \vee \min_{j' \in
        \mu(i)} V_{ij'} \]      
    \item Opportunity cost of remaining in match
      \[ \underline{V}_{ij} \equiv \max_{i' \in I: V_{i'j} > \min_{j'
          \in \mu(i')} V_{i'j'}} V_{i'j} \vee \min_{j' \in J: V_{ij'}
        > V_{\mu{j'}j'}} V_{ij'} \]
    \item $\mu$ is stable $\Leftrightarrow$ $V_{ij} <
      \overline{V}_{ij} \forall ij \not\in \mu$ $\Leftrightarrow$ $V_{ij} >
      \overline{V}_{ij} \forall ij \in \mu$ 
    \item Define $\Gamma_\mu$ as set of all valuations such that $\mu$
      is stable
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Empirical model}
  \begin{itemize}
  \item Observe $\mu$, investor and company characteristics $W_{ij}$,
    $X_{ij}$, outcomes $IPO_{ij}$
  \item $V_{ij} = W_{ij}'\alpha + \eta_{ij}$  
  \item Likelihood of matches: $\Pr(\mu \in \Gamma_\mu - W\alpha)$ 
  \item Outcome: $IPO_{ij} = 1\{ X_{ij} \beta + \epsilon_{ij} > 0 \}$
  \item Assume $(\epsilon, \eta) \sim N$
  \item Estimate using MCMC
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[height=0.75\textheight,keepaspectratio=true]{figs/sorenson-fig5}
\end{frame}

\begin{frame}\frametitle{Conclusions}
  \begin{itemize}
  \item This paper: use a matching model to correct for selection
  \item Focus is not necessarily matching by itself
  \item Does not look at efficiency of matching or any counterfactuals
    related to matching
  \item Related work: 
    \begin{itemize}
    \item \citet{park2012}: mutual fund mergers, very similar approach
    \item \citet{uetake2012}: bank mergers following deregulation,
      moment inequalities based on match stability
    \end{itemize}
  \end{itemize}  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{\citet{fox2010}}

\begin{frame}\frametitle{\citet{fox2010} ``Estimating matching games
    with transfers''}
  \begin{itemize}
  \item Context: Car parts suppliers and automotive assemblers
  \item Goal: estimate revenues from producing different portfolios of
    parts
  \item Motivating examples:
    \begin{itemize}
    \item GM considered divesting Opel -- potential loss to suppliers
      who would not gain as much from specializing
    \item Asian assembly plants enter North America -- how beneficial to
      North American parts suppliers?
    \end{itemize}    
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\citet{fox2010} ``Estimating matching games
    with transfers''}
  \begin{itemize}
  \item Data: part suppliers and assembler matches
    \begin{itemize}
    \item No observed prices 
    \end{itemize}
  \item Approach: use equilibrium conditions of matching model to
    identify revenue function
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model}
  \begin{itemize}
  \item Two-sided, many-to-many
    \begin{tabular}{l cc}
      \hline
      & Upstream & Downstream \\ \hline
      Example & Parts & Assemblers \\
      Characteristics & $\tilde{u} \in \tilde{\mathcal{U}}$ &
      $\tilde{d} \in \tilde{\mathcal{D}}$ \\ 
      Transfer & $t$ & $-t$ \\ \hline 
    \end{tabular}
  \item Full match: $\langle \tilde{u}, \tilde{d}, t \rangle$
  \item Physical matches $M = \{ \langle u_1, d_1 \rangle , ..., \langle r_n, d_n
    \rangle \}$
    \begin{itemize}
    \item $M_u = $ set of $d$ matched with $u$
    \item $M_d = $ set of $u$ matched with $d$
    \end{itemize}
  \item Revenue functions $ r^{\mathrm{up}} (M_u)$,
    $r^{\mathrm{down}}(M_d)$
  \item Equilibrium: pairwise stability $\Rightarrow$
    \[ r^{\mathrm{up}} (M_{u_1}) + t_{u_1,d_1} \geq r^{\mathrm{up}}
    \left(M_{u_1} \setminus \langle u_1, d_1 \rangle \cup \langle u_1, d_2
      \rangle\right) + t_{u_1,d_2} \]
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimator}
  \begin{itemize}
  \item Add pairwise stability conditions to eliminate transfers: 
    \begin{align*}
      r^{\mathrm{up}} (M_{u_1}) & +  r^{\mathrm{down}} (M_{d_1})+ 
      r^{\mathrm{up}} (M_{u_1}) +  r^{\mathrm{down}} (M_{d_1}) \geq \\ 
      \geq &  r^{\mathrm{up}}
      \left(M_{u_1} \setminus \langle u_1, d_1 \rangle \cup \langle u_1, d_2
        \rangle\right) + \\ & +
      r^{\mathrm{down}}
      \left(M_{d_1} \setminus \langle u_1, d_1 \rangle \cup \langle u_2, d_1
        \rangle\right) + \\ & +
      r^{\mathrm{up}}
      \left(M_{u_2} \setminus \langle u_2, d_2 \rangle \cup \langle u_2, d_1
        \rangle\right) + \\ & + r^{\mathrm{down}}
      \left(M_{d_2} \setminus \langle u_2, d_2 \rangle \cup \langle u_1, d_2
        \rangle\right)
    \end{align*}
  \item Use to form maximum score estimator
    \begin{itemize}
    \item Assume $r(M) = Z(M)'\beta$
    \item Then inequalities $\Leftrightarrow$
      $X_{u_1,u_2,d_1,d_2}'\beta \geq 0$
    \item Objective:
      \[ Q_H(\beta) = \frac{1}{H} \sum_{h \in H} \sum_{u,d} 1\{
      X_{u_1,u_2,d_1,d_2}'\beta \geq 0 \} \]
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Empirical specification}
  \begin{itemize}
  \item Data from SupplierBusiness on 1252 suppliers, 14 assemblers
    with 52 brads, 392 models, and 52492 parts in 187 component
    categories
    \begin{itemize}
    \item Data includes mainly North American and European firms
    \end{itemize}
  \item Observation: $\langle 
    \underbrace{u}_{\begin{array}{c}\text{part supplier} \\
        \text{Federal-Mogul}\end{array}},
    \underbrace{d}_{\begin{array}{c}\text{car model} \\
        \text{Fiat} \end{array}},
    \underbrace{l}_{\begin{array}{c}\text{part} \\
        \text{front pads}\end{array}} \rangle$  in market
    $\underbrace{h}_{\begin{array}{c} \text{component} \\ \text{disk
          brakes}
      \end{array}}$
  \item Revenue function: 
    \begin{itemize}
    \item $r^{\mathrm{up}}(M) = Z^{\mathrm{up}}(M)'
      \beta^{\mathrm{up}}$
    \item $ Z^{\mathrm{up}}(M) = $ measure of specialization of
      supplier in its matches, specifically HHI of parts across
      \begin{itemize}
      \item Assemblers, brands, models, continents
      \end{itemize}
    \item     \item $r^{\mathrm{down}}(M) = Z^{\mathrm{down}}(M)'
      \beta^{\mathrm{down}}$
    \item $ Z^{\mathrm{down}}(M) = $ measure of specialization of
      parts in its matches, specifically HHI of suppliers across
      \begin{itemize}
      \item Assemblers, brands, models
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Results}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/fox-table3}
\end{frame}

\begin{frame}\frametitle{Results}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/fox-table4}
\end{frame}

\begin{frame}\frametitle{Results}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/fox-table5}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Other applications and extensions}
  \begin{itemize}
  \item Identification: \citet{fox2010id}
  \item Unobserved heterogeneity: \citet{fox2012}
  \item Matching maximum score estimator of \citet{fox2010} used in 
    \begin{itemize}
    \item \citet{fox2013}: FCC spectrum auction -- no trades after
      auction implies pairwise stability
    \item \cite{akkus2012}: bank mergers, matching with observed
      transfers
    \item \citet{levine2009}: pharmaceutical marketing firms and drugs
    \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\citet{fox2013}}

\begin{frame}\frametitle{\citet{fox2013} ``Measuring the Efficiency of an FCC Spectrum
    Auction''}
  \begin{itemize}
  \item Estimates an auction model using pairwise stability
  \item C block FCC spectrum auctions 1995-1996
    \begin{itemize}
    \item Simultaneous ascending auctions for 480 geographic areas
    \item Theory \& evidence from other FCC auctions suggests collusion
    \end{itemize}
  \item Goal: estimate distribution of valuations \& and allocative
    efficiency 
  \item Identifying assumption: allocation of licenses is pairwise stable in matches,
    that is, an exchange of two licenses by winning bidders must not raise
    the sum of the valuations of the two bidders
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Background}
  \begin{itemize}
  \item C block 1900 MHz spectrum used for mobile phones
  \item Auction format:
    \begin{itemize}
    \item Multiple rounds
    \item Each round, simultaneously submit bids (or not) on all 480
      regions
    \item Auction ends when no more bids placed on any item in a round
    \item Lasted 185 days
    \end{itemize}
  \item Only new carriers participated (small business discount)
  \item 255 bidders, 85 winners, most either went bankrupt or merged
    with incumbent carriers
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fb-fig1}
\end{frame}

\begin{frame}\frametitle{Suggestive evidence of intimidatory collusion}
  \includegraphics[width=\textwidth]{fb-fig3}
\end{frame}

\begin{frame}\frametitle{Valuations}
  \begin{itemize}
  \item $a = 1,.., N$ bidders, $j = 1, .., L$ licenses
  \item Profit of bidder $a$ from $J \subset L$
    \[ \pi_a(J) - \sum_{j \in J} p_j \]
  \item Parameterization:
    \[ \pi_a(J) = \underbrace{\bar{\pi}_\beta(w_a,x_J)}_{\pm 1 \cdot elig_a \cdot
      (\sum_{j in J} pop_j) + \beta' complem_j} + \sum_{j \in J}  \xi_j +
    \sum_{j \in J} \epsilon_{a,j} \]
  \item $\bar{\pi}(w,x)$ and $\xi_j$ common knowledge of bidders,
    $\xi_j$ unobserved by econometrician
  \item $\epsilon$ i.i.d., private for bidders, unobserved by econometrician
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Measuring complementarities}
  \includegraphics[width=\textwidth]{fb-tab2}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Each bidder makes a payment before the auction begins for
    initial eligibility. A bidder’s eligibility is expressed in units
    of total population. A bidder cannot bid on a package of licenses
    that exceeds the bidder’s eligibility.
  \item $geomcomplem_J = \sum_{i \in J} pop_i 
    \frac{\sum_{j \in J
        \setminus\{i\}} \frac{pop_i pop_j}{dist_{i,j}^\delta}}
    {\sum_{j \in L
        \setminus\{i\}} \frac{pop_i pop_j}{dist_{i,j}^\delta}}$
  \item $travelcomplem_J = \sum_{i \in J} pop_i 
    \frac{\sum_{j \in J \setminus\{i\}} \text{trips(origin i,
        destination j)}}
    {\sum_{j \in L \setminus\{i\}} \text{trips(origin i,
        destination j)}}$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Pairwise stability}
  \begin{itemize}
  \item Pairwise stable in matches:
    \[ \pi_a(J_a) + \pi_b(J_b) \geq \pi_a\left( (J_a \setminus\{i_a\})
      \cup \{i_b\} \right) + \pi_b\left( (J_b \setminus\{i_b\})
      \cup \{i_a\} \right) \]
  \item Evidence for:
    \begin{itemize}
    \item Often satisfied in experimental data
    \item Swaps did not occur after auction
    \item Holds in theoretical models of ascending auctions with
      collusion: Brusco \& Lopomo (2002) and Engelbrecht-Wiggans \&
      Kahn (2005) and Milgrom (200)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item Similar to \cite{fox2010}, but without transfers
  \item Objective function $=$ sum of indicators for pairwise
    stability inequalities
  \item Fixed effects drop out of pairwise stability conditions from
    differencing 
  \item Inference through subsampling
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fb-tab3}
\end{frame}

\begin{frame}\frametitle{Interpretation}
  \begin{itemize}
  \item SD of $elig\cdot(\sum pop)$ 0.029, SD of $geocomplem$ is 0.024
  \item $\beta_{geo} = 0.32$, so $geocomplem$ 32\% as important as population
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fb-tab4}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item 6.7 and 9.8 are implausibly large --- imply increasing
    complementarity is worth as much as having 6 times the population
    in the area 
  \item Model with prices implies value of nationwide license is \$360
    billion, but total bids where \$10 billion, annual revenues in
    2006 were \$113 billion
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fb-tab5}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}